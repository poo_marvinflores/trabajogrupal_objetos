package trabajogrupal_objetos;
public abstract class Persona {
    private String nombre;
    private String identidad;
    private int edad;
    private String estadoCivil;

    public Persona(String nombre, String identidad, int edad, String estadoCivil) {
        this.nombre = nombre;
        this.identidad = identidad;
        this.edad = edad;
        this.estadoCivil = estadoCivil;
    }
    /*Metodos Abstractos*/
    public abstract void imprimir();
    public abstract String esMayorEdad();

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getIdentidad() {
        return identidad;
    }
    public void setIdentidad(String identidad) {
        this.identidad = identidad;
    }
    public int getEdad() {
        return edad;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }
    public String getEstadoCivil() {
        return estadoCivil;
    }
    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    } 
}
