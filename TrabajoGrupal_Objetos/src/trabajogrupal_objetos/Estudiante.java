package trabajogrupal_objetos;
public class Estudiante extends Persona{
    private String nivel;
    private String carrera;
    public Estudiante(String nombre, String identidad, int edad, String estadoCivil) {
        super(nombre, identidad, edad, estadoCivil);
    } 
    public Estudiante(String nivel, String carrera, String nombre, String identidad, int edad, String estadoCivil) {
        super(nombre, identidad, edad, estadoCivil);
        this.nivel = nivel;
        this.carrera = carrera;
    }
    public void imprimir(){
        System.out.println(
        "----ESTUDIANTE----\nNombre: "+this.getNombre()+"\nIdentidad: "+this.getIdentidad()
        +"\nEdad: "+this.getEdad()+"\nEstado Civil: "+this.getEstadoCivil()+"\nEstado: "+this.esMayorEdad()+"\nDatos Institucion"
         +"\nCarrera: "+this.getCarrera()+"\nGrado: "+this.getNivel()
        );
    }
    public String esMayorEdad(){
        return this.getEdad()>18?"Es Mayor de Edad":"Es Menor de Edad";
    }
    public String getNivel() {
        return nivel;
    }
    public void setNivel(String nivel) {
        this.nivel = nivel;
    }
    public String getCarrera() {
        return carrera;
    }
    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
}

/**/