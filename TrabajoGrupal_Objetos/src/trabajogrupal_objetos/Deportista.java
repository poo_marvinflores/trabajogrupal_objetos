package trabajogrupal_objetos;
public class Deportista extends Persona{
   private String deporte;
    public Deportista(String nombre, String identidad, int edad, String estadoCivil,String deporte) {
        super(nombre, identidad, edad, estadoCivil);
        this.deporte=deporte;
    }
    public void imprimir(){
        System.out.println("----DEPORTISTA----\nNombre: "+this.getNombre()+"\nIdentidad: "+this.getIdentidad()+"\nEdad: "+this.getEdad()+"\nEstado Civil: "+this.getEstadoCivil()
        +"\nEstado: "+this.esMayorEdad()+"\nDeporte que practica: "+this.getDeporte());
    }
    public String esMayorEdad(){
        if(this.getEdad()>18){
            return "Mayor de edad";
        }else{
            return "Menor de Edad";
        }
    }
    public String getDeporte() {
        return deporte;
    }
    public void setDeporte(String deporte) {
        this.deporte = deporte;
    }  
}
