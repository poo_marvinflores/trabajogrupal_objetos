package trabajogrupal_objetos;
public class Main {
    public static void main(String args[]){
        Persona pers[];
        pers=new Persona[4];
        pers[0]=new Estudiante("2-Aº", "CBT", "Marvin", "0208199400972", 25, "Soltero");
        pers[1]=new Deportista("Rigoberto Avila", "0704199300310", 5, "Soltero","Futboll");
        pers[2]=new Estudiante("1-Bº", "BTP-Informatica", "Wilson Murillo", "0101199100950", 28, "Soltero");
        pers[3]=new Deportista("Arnol Gutierrez", "0151210122", 31, "Casado","Natacion");
        for(int x=0;x<pers.length;x++)
        {
              pers[x].imprimir();//Aplicamos polimorfismo en esta clase
              //ya que por medio de la clase abstracta logramos comunicar todas las clases interconectadas a la clase principal.
              System.out.println("------------------------------------------");
        }
    }
}
/*
-----------INTEGRANTES-----------
1._ Marvin Flores - 201610020092
2._ Rigoberto Avila - 201610020264
3._Wilson Murillo - 201320020051
*/